# Mon premier dépôt Git Ceci est mon premier dépôt.

## Liste des commandes
- `git init` : initialise le dépôt
- `git add` : ajoute un fichier à la zone d'index
- `git commit` : valide les modifications indexées dans la zone d'index
- `git status` : permet de voir l'état du dépôt
- `git log` : affiche l'historique

- `git remote add mon_depot_distant git@gitlab.com:[votre login]/[slug du dépôt]` : pour 
ajouter le dépôt GitLab nouvellement créé comme remote avec le nom mon_depot_distant
- `git push mon_depot_distant master ` : Cette commande « pousse » (copie) la branche
locale actuelle sur la branche master du remote mon_depot_distant
- `git branch master --set-upstream-to=mon_depot_distant/master` : 
pour que la branche locale master suive automatiquement la
branche master du remote mon_depot_distant.Ceci permet maintenant d'utiliser git push à
la place de git push mon_depot_distant master
- `git clone [lien SSH ou HTTPS]` : permet de cloner un dépôt distant en local
